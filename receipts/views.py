from django.shortcuts import render, redirect
from django.views.generic import ListView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin

# from django.contrib.auth.decorators import login_required
# from django.views.decorators.http import require_http_methods

from receipts.models import Account, ExpenseCategory, Receipt


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/list.html"

    def get_queryset(self):
        purchaser_receipts = Receipt.objects.filter(
            purchaser=self.request.user
        )
        return purchaser_receipts


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name = "receipts/create.html"
    fields = ["vendor", "total", "tax", "date", "category", "account"]

    # def get_queryset(self):
    #     return Receipt.objects.filter(purchaser=self.request.user)

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     context["account"] = Receipt.objects.filter(
    #         owner=self.request.user
    #     )
    #     return context

    def get_form(self, *args, **kwargs):
        form = super(ReceiptCreateView, self).get_form(*args, **kwargs)
        form.fields["account"].queryset = Account.objects.filter(
            owner=self.request.user
        )
        form.fields["category"].queryset = ExpenseCategory.objects.filter(
            owner=self.request.user
        )
        return form

    def form_valid(self, form):
        receipt = form.save(commit=False)
        receipt.purchaser = self.request.user
        receipt.save()
        return redirect("home")


class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = "receipts/expense_list.html"

    def get_queryset(self):
        user_expense_categories = ExpenseCategory.objects.filter(
            owner=self.request.user
        )
        return user_expense_categories


class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "receipts/accounts_list.html"

    def get_queryset(self):
        user_accounts = Account.objects.filter(owner=self.request.user)
        return user_accounts


class ExpenseCategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template_name = "receipts/expense_create.html"
    fields = ["name"]

    def form_valid(self, form):
        category = form.save(commit=False)
        category.owner = self.request.user
        category.save()
        return redirect("list_categories")


class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    template_name = "receipts/account_create.html"
    fields = ["name", "number"]

    def form_valid(self, form):
        account = form.save(commit=False)
        account.owner = self.request.user
        account.save()
        return redirect("account_list")
