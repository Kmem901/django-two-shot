from django.contrib import admin
from receipts.models import Receipt, Account, ExpenseCategory

admin.site.register(Receipt)
admin.site.register(Account)
admin.site.register(ExpenseCategory)
# Register your models here.
